#!/usr/bin/env python

import os
os.environ['HOME']="/tmp"

from dolfin import *
from mshr import *
import sys
import random

#input_line = sys.stdin.read()
#input_line.strip()
#[par1, par2] = input_line.split()
#par1 = float(par1)
#par2 = float(par2)

x0=random.uniform(0.2, 0.8)
y0=random.uniform(0.2, 0.8)

resolution = 300

domain =   Rectangle(Point(0.0, 0.0), Point(1.0, 1.0)) - Circle( Point(x0, y0), 0.15)

mesh = generate_mesh(domain, resolution)
domains = dolfin.MeshFunction("size_t", mesh, 2, mesh.domains())
#plot(domains, "Subdomains")
#interactive()


# Create classes for defining parts of the boundaries and the interior
# of the domain
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return (near(x[0], 0.0) and on_boundary)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return (near(x[0], 1.0) and on_boundary)

left = Left()
right = Right()

# Initialize mesh function for boundary domains
boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(0)
left.mark(boundaries, 1)
right.mark(boundaries, 2)

# Define input data
a0 = Constant(1.0)
voltage = 1.0

# Define function space and basis functions
V = FunctionSpace(mesh, "CG", 1)
u = TrialFunction(V)
v = TestFunction(V)

# Define Dirichlet boundary conditions at top and bottom boundaries
bcs = [DirichletBC(V, voltage, boundaries, 2),
       DirichletBC(V, 0.0, boundaries, 1)]

# Define new measures associated with the interior domains and
# exterior boundaries
dx = Measure("dx")(subdomain_data=domains)
ds = Measure("ds")(subdomain_data=boundaries)
f = Constant(0.0)



# Define variational form
a = inner(a0*grad(u), grad(v))*dx
L = f*v*dx()

# Solve problem
u = Function(V)

# Define goal functional (quantity of interest)
#M = u*dx()
#tol = 1.e-4
#problem = LinearVariationalProblem(a, L, u, bcs)
#solver = AdaptiveLinearVariationalSolver(problem, M)
#solver.parameters["error_control"]["dual_variational_solver"]["linear_solver"] = "cg"
#solver.solve(tol)

solve(a == L, u, bcs)

# R = U/I
if(0):
	n = FacetNormal(mesh)
	m1 = a0*dot(grad(u), n)*ds(1)
	I1 = assemble(m1)
	print I1
	n = FacetNormal(mesh)
	m2 = a0*dot(grad(u), n)*ds(2)
	I2 = assemble(m2)
	print I2
	current = (I2+(-I1))/2
	print current
	R = voltage/current

# R = U*U/P
if(1):
	energy = a0*inner(grad(u), grad(u))*dx
	power = assemble(energy)
	#print "Power total:", power
	R = voltage*voltage/power 

print x0, y0, R


# Plot solution and gradient
#plot(u, title="u")
#plot(grad(u), title="Projected grad(u)")
#interactive()
