import sys
import signal
from threading import Thread
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

class PUTHandler(BaseHTTPRequestHandler):
    def do_PUT(self):
        length = int(self.rfile.readline(),16)
        content = self.rfile.read(length)
        self.send_response(200)
        if( self.path == '//_condor_stdout'):
            output.write(content)
            print length
            print content

def run_on(host,port):
    print "Starting a server on %s:%i" % (host,port) 
    server_address = (host, port)
    httpd = HTTPServer(server_address, PUTHandler)
    httpd.serve_forever()

if __name__ == "__main__":
    output = open('server_data','w')
    server = Thread(target=run_on, args=['n1', 4321])
    server.daemon = True # Do not make us wait for you to exit
    server.start()
    signal.pause() # Wait for interrupt signal, e.g. KeyboardInterrupt
    
