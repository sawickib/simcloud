#!/usr/bin/env python

import sys
from subprocess import Popen, PIPE, STDOUT

# input comes from STDIN (standard input)
for line in sys.stdin:
   
    p = Popen(['agros2d_solver', '-s', 'solver.py'], stdout=PIPE,  stdin=PIPE, stderr=STDOUT)
    result = p.communicate(input=line)[0].strip();

    print '%s\t%s' % (result, 1)


