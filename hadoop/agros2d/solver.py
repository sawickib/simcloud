import agros2d 
import sys

input_line = sys.stdin.read()
input_line.strip()
[mat1, mat2] = input_line.split()
mat1 = float(mat1)
mat2 = float(mat2)

agros2d.open_file("box-in-box.a2d")
problem = agros2d.problem()
current = agros2d.field("current")
current.modify_material("mat1",{"current_conductivity" : mat1})
current.modify_material("mat2",{"current_conductivity" : mat2})
problem.solve()

p = current.volume_integrals([1])["Pj"]
print "%.2f"% p


