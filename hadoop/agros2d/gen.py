#!/usr/bin/env python

import random

file = open("input.txt","w")


def uniform():
  minv1 = 1.0
  maxv1 = 2.0
  resolution1 = 5
  minv2 = 1.0
  maxv2 = 2.0
  resolution2 = 5
  for i in range (resolution1+1):
    for j in range (resolution2+1):
      v1 = minv1 + i*(maxv1-minv1)/resolution1
      v2 = minv2 + j*(maxv2-minv2)/resolution2
      file.write("%f %f\n" % (v1, v2))

def gauss(trials):
  for i in range(trials):
    v1 = random.gauss(1.0, 0.1)
    v2 = random.gauss(10.0, 1.0)
    file.write("%f %f\n" % (v1, v2))
  

gauss(100)

