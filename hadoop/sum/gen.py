#!/usr/bin/env python

import random

file = open("random.txt","w")

for i in range (1000000):
  file.write("%d " % random.randint(1,1000))
  file.write("%d " % random.randint(1,1000))
  file.write("%d " % random.randint(1,1000))
  file.write("\n")
