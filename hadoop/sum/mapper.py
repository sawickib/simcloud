#!/usr/bin/env python

import sys

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    words = line.split()
   
    result = int(words[0])+int(words[1])+int(words[2])

    print '%d\t%s' % (result, 1)

