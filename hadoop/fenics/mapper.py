#!/usr/bin/env python

import sys
from subprocess import Popen, PIPE, STDOUT

for input_line in sys.stdin:
   
    p = Popen(['python', 'solver.py'], stdout=PIPE,  stdin=PIPE, stderr=STDOUT)
    output = p.communicate(input=input_line)[0];
    output_lines = output.split('\n')

    result = 0.0
    for output_line in output_lines:
        try:
            result = float(output_line.strip())
        except:
            pass
    

    param =  input_line.strip().split()
    print '0\t%f %s,%s' % (result, param[0], param[1])


