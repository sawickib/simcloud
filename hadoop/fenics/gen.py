#!/usr/bin/env python

import random

file = open("input.txt","w")

minv1 = 0.1
maxv1 = 0.8
resolution1 = 5
minv2 = 0.05
maxv2 = 0.85
resolution2 = 5

def uniform():
  for i in range (resolution1+1):
    for j in range (resolution2+1):
      v1 = minv1 + i*(maxv1-minv1)/resolution1
      v2 = minv2 + j*(maxv2-minv2)/resolution2
      file.write("%f %f\n" % (v1, v2))

def montecarlo(trials):
  for i in range(resolution1*resolution2):
    v1 = random.uniform(minv1, maxv1)
    v2 = random.uniform(minv2, maxv2)
    file.write("%f %f\n" % (v1, v2))

def gauss(trials):
  for i in range(trials):
    v1 = random.gauss(1.0, 0.1)
    v2 = random.gauss(10.0, 1.0)
    file.write("%f %f\n" % (v1, v2))
  
montecarlo(100)
#gauss(100)

