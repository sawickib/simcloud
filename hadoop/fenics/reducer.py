#!/usr/bin/env python

import sys

max = float("-inf")
max_param = ""
min = float("+inf")
min_param = ""

for line in sys.stdin:
    line = line.strip()

    values = line.split('\t')[1].split()
    
    result = float(values[0])
    param = values[1]

    if result > max:
        max = result
        max_param = param

    if result < min:
        min = result
        min_param = param

print 'min\t%f %s' % (min, min_param)
print 'max\t%f %s' % (max, max_param)

